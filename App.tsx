import NavigationProvider from 'navigation'
import React from 'react'

const App = () => {
  return (
    <>
      <NavigationProvider />
    </>
  )
}

export default App
