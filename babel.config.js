module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        alias: {
          assets: './src/assets',
          constants: './src/constants',
          components: './src/components',
          styles: './src/styles',
          interface: './src/interface',
          navigation: './src/navigation',
          modules: './src/modules',
          services: './src/services',
          utils: './src/utils',
        },
      },
    ],
  ],
}
