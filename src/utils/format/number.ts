export const formatNumber = (
  number: string | number,
  length?: number | undefined,
  numericFormatting?: boolean,
) => {
  const lengthOfNumber = `${number}`.length;
  const stringNumber = `${number}`;

  if (
    lengthOfNumber >= 4 &&
    lengthOfNumber < 7 &&
    numericFormatting !== true
  ) {
    return `${stringNumber.slice(0, lengthOfNumber - 3)}.${stringNumber.slice(
      3
    )}k`;
  } else
    if (
      lengthOfNumber >= 7 &&
      lengthOfNumber < 10 &&
      numericFormatting !== true
    ) {
      const extent = lengthOfNumber - 6;
      return `${stringNumber.slice(0, extent)}.${stringNumber.slice(
        extent,
        extent + 3,
      )} M`;
    } else if (lengthOfNumber >= 10 && numericFormatting !== true) {
      const extent = lengthOfNumber - 9;

      return `${stringNumber.slice(0, extent)}.${stringNumber.slice(
        extent,
        extent + 3,
      )} B`;
    } else {
      const parsedNumber = parseFloat(stringNumber).toFixed(length ?? 0);
      var parts = parsedNumber.toString().split('.');
      parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
      return parts.join('.');
    }
}