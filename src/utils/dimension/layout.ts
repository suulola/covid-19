import { Dimensions, PixelRatio, Platform } from "react-native";

const { width } = Dimensions.get("window");
const { height } = Dimensions.get("window");

// const scale = PixelRatio.getFontScale();

// Based on iPhone 11 scale
const widthBaseScale = width / 414;
const heightBaseScale = height / 896;

export function normalize(size: number, based: "width" | "height" = "width") {
  // const newSize = size * scale;
  const newSize =
    based === "height" ? size * heightBaseScale : size * widthBaseScale;
  if (Platform.OS === 'ios') {
    return Math.round(PixelRatio.roundToNearestPixel(newSize));
  }
  return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2;
}

//for width  pixel
export const widthPixel = (size: number) => {
  return normalize(size, "width");
};
//for height  pixel
export const heightPixel = (size: number) => {
  return normalize(size, "height");
};
//for font  pixel
export const fontPixel = (size: number) => {
  return heightPixel(size);
};
//for Margin and Padding vertical pixel
export const pixelSizeVertical = (size: number) => {
  return heightPixel(size);
};
//for Margin and Padding horizontal pixel
export const pixelSizeHorizontal = (size: number) => {
  return widthPixel(size);
};
