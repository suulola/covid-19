import { PURE_WHITE, APP_DARK_BLACK } from "styles/constant";

export const chartConfig = {
  backgroundGradientFrom: PURE_WHITE,
  backgroundGradientTo: PURE_WHITE,
  fillShadowGradientOpacity: 0.2,
  color: (opacity = 1) => `rgba(108, 226, 30, ${opacity})`,
  labelColor: (opacity = 1) => APP_DARK_BLACK,
  strokeWidth: 2,
  useShadowColorFromDataset: false, // optional
  decimalPlaces: 2,
  barPercentage: 0.5,

  style: {
    borderRadius: 16,
  },
  propsForBackgroundLines: {
    strokeWidth: 0,
  },
}

