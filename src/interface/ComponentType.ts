export interface NavigationType {
  navigate: (route: any, option?: any) => void;
  push: (route: string, option?: any) => void;
  goBack: () => void;
}


export interface ComponentType {
  navigation: NavigationType,
  route?: { params: any };

}