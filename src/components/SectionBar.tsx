import {Image, ImageSourcePropType, Pressable, View} from 'react-native'
import React from 'react'
import {flex} from 'styles/layout'
import {height, margin, padding, width} from 'styles/spacing'
import CustomText from './CustomText'
import {font, font_size} from 'styles/typography'

interface SectionBarType {
  title: string
  subText?: string
  onPress?: () => void
  image?: ImageSourcePropType
}

const SectionBar = ({title, subText, onPress, image}: SectionBarType) => {
  return (
    <View style={[flex.row, flex.justifyBetween, margin.t_20, padding.l_5]}>
      <CustomText text={title} textStyle={[font_size[17], font.bold]} />
      <Pressable onPress={onPress} style={[flex.row, flex.alignCenter]}>
        {subText && (
          <CustomText text={subText} textStyle={[font_size[12], font.bold]} />
        )}
        {image && <Image source={image} style={[width[20], height[20]]} />}
      </Pressable>
    </View>
  )
}

export default SectionBar
