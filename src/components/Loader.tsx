import React from 'react'
import {ActivityIndicator, ColorValue, View} from 'react-native'
import {APP_BLUE} from 'styles/constant'
import {flex} from 'styles/layout'
import {margin, width} from 'styles/spacing'
import CustomText from './CustomText'

interface LoaderType {
  size?: number | 'large' | 'small' | undefined
  loading: boolean
  color?: ColorValue
  centralize?: boolean
  emptyDisplayMessage?: string
}

const Loader = ({
  size,
  centralize,
  color,
  loading,
  emptyDisplayMessage,
}: LoaderType) => {
  return (
    <View
      style={[
        size === 'large' || centralize
          ? [flex.flex_1, margin.t_30, flex.alignCenter]
          : [width[36]],
      ]}
    >
      {loading ? (
        <ActivityIndicator size={size ?? 'small'} color={color ?? APP_BLUE} />
      ) : (
        <CustomText text={emptyDisplayMessage ?? `No data`} />
      )}
    </View>
  )
}

export default Loader
