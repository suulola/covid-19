import {TextStyle, TouchableOpacity, ViewStyle} from 'react-native'
import React from 'react'
import CustomText from './CustomText'
import {StyleProp} from 'react-native'
import {color, font_size} from 'styles/typography'
import {padding, width} from 'styles/spacing'
import {background} from 'styles/background'
import {border, flex} from 'styles/layout'

interface ButtonInterface {
  title: string
  onPress: () => void
  textStyle?: StyleProp<TextStyle>
  containerStyle?: StyleProp<ViewStyle>
}

const CustomButton = ({
  title,
  onPress,
  textStyle,
  containerStyle,
}: ButtonInterface) => {
  return (
    <TouchableOpacity onPress={onPress} style={[styles.button, containerStyle]}>
      <CustomText
        textStyle={[color.text_white, font_size[12], textStyle]}
        text={title}
      />
    </TouchableOpacity>
  )
}

export default CustomButton

const styles = {
  button: [
    width[128],
    background.blue,
    padding.y_10,
    flex.alignCenter,
    flex.justifyCenter,
    border.r_8,
    padding.x_5,
  ],
}
