import {Image, Pressable, View, StyleProp, ViewStyle} from 'react-native'
import React from 'react'
import {background} from 'styles/background'
import {border, flex} from 'styles/layout'
import {padding, width, height, margin} from 'styles/spacing'
import {font_size} from 'styles/typography'
import {menu, edit, back} from 'utils/images/list'
import CustomText from './CustomText'

interface AppbarInterface {
  title?: string
  containerStyle?: StyleProp<ViewStyle>
  showBack?: boolean
  onPress?: () => void
  showExtra?: boolean
}

const Appbar = ({
  title = ``,
  containerStyle,
  showBack,
  onPress,
  showExtra,
}: AppbarInterface) => {
  return (
    <View style={[styles.container, containerStyle]}>
      <View style={[flex.row, flex.alignCenter]}>
        <Pressable onPress={onPress}>
          <Image
            source={showBack ? back : menu}
            style={[width[22], height[17]]}
          />
        </Pressable>
        <CustomText text={title} textStyle={[font_size[24], margin.l_10]} />
      </View>
      {showExtra !== false && (
        <Pressable onPress={onPress} style={[styles.button]}>
          <Image
            resizeMode={`contain`}
            source={edit}
            style={[width[20], height[20]]}
          />
        </Pressable>
      )}
    </View>
  )
}

export default Appbar

const styles = {
  container: [flex.row, flex.justifyBetween, padding.t_20, padding.b_10],
  button: [
    flex.justifyCenter,
    flex.alignCenter,
    background.pure_white,
    width[36],
    height[36],
    border.r_10,
    border.w_1,
    border.c_blue,
  ],
}
