import {Pressable, StyleProp, StyleSheet, TextStyle, View} from 'react-native'
import React from 'react'
import {border, flex} from 'styles/layout'
import CustomText from './CustomText'
import {height} from 'styles/spacing'
import Divider from './Divider'
import {align, color, font} from 'styles/typography'

interface TableRowType {
  firstText: string
  secondText: string
  thirdText: string
  firstStyle?: StyleProp<TextStyle>
  secondStyle?: StyleProp<TextStyle>
  thirdStyle?: StyleProp<TextStyle>
  onPress?: () => void
}

const TableRow = ({
  firstText,
  secondText,
  thirdText,
  firstStyle,
  secondStyle,
  thirdStyle,
  onPress,
}: TableRowType) => {
  return (
    <View style={[height[49]]}>
      <Pressable
        onPress={onPress}
        style={[flex.row, flex.justifyBetween, flex.flex_1, flex.alignCenter]}
      >
        <CustomText
          text={firstText}
          textStyle={[styles.firstColumn, font.semiBold, firstStyle]}
        />
        <CustomText
          text={secondText}
          textStyle={[flex.flex_1, align.center, color.text_green, secondStyle]}
        />
        <CustomText
          text={thirdText}
          textStyle={[align.center, flex.flex_1, color.text_red, thirdStyle]}
        />
      </Pressable>
      <Divider />
    </View>
  )
}

export default TableRow

const styles = StyleSheet.create({
  firstColumn: {
    flex: 1.5,
  },
})
