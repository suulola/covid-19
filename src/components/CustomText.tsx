import React from 'react'
import {Text, StyleProp, TextStyle} from 'react-native'
import {color, font} from 'styles/typography'

interface ICustomText {
  text: string
  textStyle?: StyleProp<TextStyle>
}

const CustomText = ({text, textStyle}: ICustomText) => {
  return <Text style={[color.text_black, font.regular, textStyle]}>{text}</Text>
}

export default CustomText
