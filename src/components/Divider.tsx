import React from 'react'
import {View, StyleProp, ViewStyle, StyleSheet} from 'react-native'
import {width, height} from 'styles/spacing'
interface IDivider {
  styles?: StyleProp<ViewStyle>
}

const Divider = ({styles}: IDivider) => {
  return (
    <View style={[[width.full, height[1]], componentStyle.divider, styles]} />
  )
}

const componentStyle = StyleSheet.create({
  divider: {
    backgroundColor: `#9EBAED`,
    opacity: 0.2,
  },
})

export default Divider
