import {Image, ImageSourcePropType, View} from 'react-native'
import React from 'react'
import {flex} from 'styles/layout'
import {padding, margin, width, height} from 'styles/spacing'
import {font_size, font, color, transform} from 'styles/typography'
import {formatNumber} from 'utils/format/number'
import {globe} from 'utils/images/list'
import CustomText from './CustomText'

interface StatisticsType {
  totalCases: number
  totalNewCases: number
  metrics: any[]
  image?: ImageSourcePropType
  title: string
}

const Statistics = ({
  totalCases,
  totalNewCases,
  metrics,
  image,
  title,
}: StatisticsType) => {
  return (
    <View style={[padding.x_20]}>
      <View style={[flex.row, flex.alignCenter, margin.b_20]}>
        {image && (
          <Image source={image} style={[width[20], height[20], margin.r_5]} />
        )}
        <CustomText text={title} />
      </View>

      <CustomText text={`Total Cases`} textStyle={[margin.y_10]} />

      <View style={[flex.row, flex.alignBase]}>
        <CustomText
          text={`${totalCases}`}
          textStyle={[font_size[24], font.bold, margin.r_2]}
        />
        <CustomText
          text={`+${totalNewCases}`}
          textStyle={[color.text_gray, font_size[12], font.semiBold]}
        />
      </View>

      <View style={[flex.row, flex.justifyBetween, padding.y_20]}>
        {metrics.map(stat => (
          <View key={stat.type}>
            <CustomText
              text={stat.type}
              textStyle={[transform.capitalize, margin.b_10, font_size[14]]}
            />
            <CustomText
              text={`${formatNumber(stat.count, 0)}`}
              textStyle={[
                font_size[17],
                font.bold,
                [
                  stat.type === 'actives'
                    ? color.text_blue
                    : stat.type === 'deaths'
                    ? color.text_red
                    : color.text_green,
                ],
              ]}
            />
            <View style={[flex.row, margin.t_5]}>
              <CustomText
                text={`New: `}
                textStyle={[color.text_gray, font_size[10]]}
              />
              <CustomText
                text={`+${formatNumber(stat.new, 0, true)}`}
                textStyle={[
                  font.light,
                  [
                    stat.type === 'actives'
                      ? color.text_blue
                      : stat.type === 'deaths'
                      ? color.text_red
                      : color.text_green,
                  ],
                  font_size[10],
                ]}
              />
            </View>
          </View>
        ))}
      </View>
    </View>
  )
}

export default Statistics

