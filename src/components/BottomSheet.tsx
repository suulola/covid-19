import React, {Fragment, useEffect, useRef} from 'react'
import {Animated, Image, Modal, Pressable, StyleSheet, View} from 'react-native'
import {PURE_WHITE} from 'styles/constant'
import CustomText from './CustomText'
import {flex} from 'styles/layout'
import {height, padding, width} from 'styles/spacing'
import {opacity} from 'styles/background'
import Divider from './Divider'
import {sortOption} from 'utils/data/values'
import {color, font} from 'styles/typography'
import {selected} from 'utils/images/list'

interface BottomSheetType {
  showBottomSheet: boolean
  setShowBottomSheet: React.Dispatch<React.SetStateAction<boolean>>
  setSelectedSort: React.Dispatch<React.SetStateAction<string>>
  selectedSort: string
}

const BottomSheet = ({
  showBottomSheet,
  setShowBottomSheet,
  setSelectedSort,
  selectedSort,
}: BottomSheetType) => {
  const fadeAnim = useRef(new Animated.Value(100)).current

  const handleSelectIndicator = (value: string) => {
    setSelectedSort(value)
    setShowBottomSheet(false)
  }

  useEffect(() => {
    const animate = () => {
      Animated.timing(fadeAnim, {
        toValue: 0,
        duration: 1000,
        useNativeDriver: true,
      }).start()
    }
    if (showBottomSheet) {
      animate()
    }
  }, [showBottomSheet])

  return (
    <Modal animationType={'slide'} transparent={true} visible={showBottomSheet}>
      <Pressable
        onPress={() => setShowBottomSheet(false)}
        style={[styles.container]}
      >
        <View style={[flex.flex_1, opacity.faint]}></View>
        <Pressable onPress={() => {}} style={[styles.innerContainer]}>
          <CustomText text={`Sort By`} textStyle={[padding.b_10, font.bold]} />
          {sortOption.map(indicator => (
            <Fragment key={indicator.value}>
              <Pressable
                onPress={() => handleSelectIndicator(indicator.value)}
                style={grouped.button}
              >
                <CustomText
                  text={indicator.label}
                  textStyle={[
                    [
                      selectedSort === indicator.value
                        ? [color.text_black]
                        : [color.text_gray],
                    ],
                  ]}
                />
                {selectedSort === indicator.value && (
                  <Image
                    resizeMode='contain'
                    source={selected}
                    style={[width[14], height[14]]}
                  />
                )}
              </Pressable>
              <Divider />
            </Fragment>
          ))}
        </Pressable>
      </Pressable>
    </Modal>
  )
}

export default BottomSheet

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-end',
    backgroundColor: '#1a1a1a73',
  },
  innerContainer: {
    opacity: 1,
    backgroundColor: PURE_WHITE,
    minHeight: '40%',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    width: '100%',
    justifyContent: 'flex-end',
    paddingBottom: '10%',
    paddingHorizontal: 30,
  },
})

const grouped = {
  button: [
    padding.t_20,
    padding.b_10,
    flex.row,
    flex.justifyBetween,
    flex.alignCenter,
  ],
}
