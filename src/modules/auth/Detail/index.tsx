import {SafeAreaView, ScrollView, View} from 'react-native'
import React from 'react'
import Appbar from 'components/Appbar'
import {background} from 'styles/background'
import {border, flex} from 'styles/layout'
import {margin, padding} from 'styles/spacing'
import {ComponentType} from 'interface/ComponentType'
import SectionBar from 'components/SectionBar'
import {forward} from 'utils/images/list'
import Statistics from 'components/Statistics'
import {metrics} from 'utils/data/values'
import {LineChartComponent} from '../Home/section/Chart'

const Detail = ({navigation, route}: ComponentType) => {
  const detail: any = route?.params?.detail

  return (
    <SafeAreaView style={[background.app_white, flex.flex_1]}>
      <Appbar
        containerStyle={[padding.x_20]}
        showBack={true}
        onPress={() => navigation.goBack()}
      />
      <ScrollView contentContainerStyle={[padding.x_20]}>
        <SectionBar
          title={`Today's Statistics`}
          subText={new Date().toDateString()}
          image={forward}
        />

        <View
          style={[
            flex.flex_1,
            background.pure_white,
            border.r_8,
            margin.t_10,
            padding.y_20,
          ]}
        >
          <Statistics
            totalCases={detail?.TotalConfirmed}
            totalNewCases={detail?.NewConfirmed}
            metrics={metrics}
            title={detail?.Country}
          />
          <ScrollView horizontal showsHorizontalScrollIndicator={false}>
            <LineChartComponent />
          </ScrollView>
        </View>
      </ScrollView>
    </SafeAreaView>
  )
}

export default Detail
