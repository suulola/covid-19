import {Dimensions} from 'react-native'
import React, {useEffect, useState} from 'react'
import {BarChart, LineChart, ProgressChart} from 'react-native-chart-kit'
import {chartData} from 'utils/data/values'
import {chartConfig} from 'utils/data/chart'

const {height} = Dimensions.get('screen')
const screenWidth = Dimensions.get('window').width

export const BarChartComponent = () => {
  const data = {
    labels: Object.keys(chartData.Global),
    datasets: [
      {
        data: Object.values(chartData.Global),
      },
    ],
  }

  return (
    <BarChart
      data={data}
      width={data.labels.length * 120}
      height={height / 2.9}
      xLabelsOffset={10}
      yAxisLabel={''}
      yAxisSuffix={``}
      chartConfig={chartConfig}
      withVerticalLabels={true}
    />
  )
}
export const LineChartComponent = () => {
  const data: any = {
    labels: Object.keys(chartData.Global),
    datasets: [
      {
        data: Object.values(chartData.Global),
        strokeWidth: 2,
      },
    ],
  }

  return (
    <LineChart
      data={data}
      width={data.labels.length * 120}
      height={height / 2.9}
      xLabelsOffset={10}
      yAxisLabel={''}
      yAxisSuffix={``}
      chartConfig={chartConfig}
      withVerticalLabels={true}
    />
  )
}

export const ProgressChartComponent = () => {
  const [data, setData] = useState({
    labels: ['Swim', 'Bike', 'Run'],
    data: [0.4, 0.6, 0.8],
  })

  useEffect(() => {
    const values = Object.values(chartData.Global)
    const sum = values.reduce((sum, currValue) => sum + currValue, 0)
    console.log({sum})
    const datasetValues = values.map(item => (item / sum) * 1)
    console.log({datasetValues})
    const result: any = {
      labels: Object.keys(chartData.Global),
      data: datasetValues,
    }
    console.log({result})
    setData(result)
  }, [])

  return (
    <ProgressChart
      data={data}
      width={screenWidth - 50}
      height={220}
      strokeWidth={16}
      radius={32}
      chartConfig={chartConfig}
      hideLegend={true}
    />
  )
}
