import {Image, View} from 'react-native'
import React from 'react'
import CustomText from 'components/CustomText'
import {background} from 'styles/background'
import {border, flex} from 'styles/layout'
import {margin, height, padding} from 'styles/spacing'
import {color, font, font_size} from 'styles/typography'
import {person} from 'utils/images/list'
import CustomButton from 'components/CustomButton'

interface PCRCardType {
  onPress: () => void
}

const PCRCard = ({onPress}: PCRCardType) => {
  return (
    <View style={[styles.container]}>
      <View style={[flex.flex_1, flex.justifyCenter, padding.l_10]}>
        <CustomText
          text={`COVID-19 Test`}
          textStyle={[color.text_white, font_size[20], font.semiBold]}
        />
        <CustomText
          text={`Get tested at the first of it's kind accredited laboratory `}
          textStyle={[color.text_white, font_size[11], padding.y_15]}
        />
        <CustomButton title={`Get a PCR Centre`} onPress={onPress} />
      </View>
      <Image
        style={[flex.flex_1, height.max_full]}
        resizeMode={'contain'}
        source={person}
      />
    </View>
  )
}

export default PCRCard

const styles = {
  container: [
    margin.t_10,
    border.r_12,
    height[175],
    background.card_bg,
    flex.row,
    padding.x_10,
    margin.t_20,
    margin.b_30,
  ],
}
