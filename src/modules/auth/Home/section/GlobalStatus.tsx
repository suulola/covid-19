import {ScrollView, View} from 'react-native'
import React from 'react'
import {background} from 'styles/background'
import {border, flex} from 'styles/layout'
import {padding, margin} from 'styles/spacing'
import CustomText from 'components/CustomText'
import {color, font, font_size} from 'styles/typography'
import Statistics from 'components/Statistics'
import {globe} from 'utils/images/list'

interface GlobalStatusType {
  children: any
  totalCases: number
  totalNewCases: number
  metrics: any[]
}

const GlobalStatus = ({
  children,
  totalCases,
  totalNewCases,
  metrics,
}: GlobalStatusType) => {
  return (
    <View style={[styles.container]}>
      <Statistics
        totalCases={totalCases}
        totalNewCases={totalNewCases}
        metrics={metrics}
        image={globe}
        title={`Global Status`}
      />
      <CustomText
        text={`Total Cases Of Coronavirus`}
        textStyle={[margin.y_10, font.medium, font_size[17]]}
      />
      <ScrollView
        horizontal
        contentContainerStyle={[padding.l_20]}
        showsHorizontalScrollIndicator={false}
      >
        {children}
      </ScrollView>
      <View style={[flex.row, padding.x_20, padding.b_20]}>
        <CustomText
          text={`Last Updated: `}
          textStyle={[color.text_gray, font_size[10]]}
        />
        <CustomText text={`Oct 5 2021`} textStyle={[font_size[10]]} />
      </View>
    </View>
  )
}

export default GlobalStatus

const styles = {
  container: [
    background.pure_white,
    border.r_12,
    border.w_thin,
    padding.y_20,
    border.c_grey,
  ],
}
