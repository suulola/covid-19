import {FlatList, Image, Pressable, SafeAreaView, View} from 'react-native'
import React, {useEffect, useState} from 'react'
import {background} from 'styles/background'
import Appbar from 'components/Appbar'
import PCRCard from './section/PCRCard'
import {border, flex} from 'styles/layout'
import {height, margin, padding, width} from 'styles/spacing'
import GlobalStatus from './section/GlobalStatus'
import CustomText from 'components/CustomText'
import {forward} from 'utils/images/list'
import {color, font} from 'styles/typography'
import SectionBar from 'components/SectionBar'
import {loadingAndErrorDefault} from 'constants/defaultState'
import Loader from 'components/Loader'
import TableRow from 'components/TableRow'
import {ComponentType} from 'interface/ComponentType'
import {COUNTRY_LIST_SCREEN, DETAIL_SCREEN} from 'navigation/constant'
import {chartData, metrics, newsData} from 'utils/data/values'
import {BarChartComponent} from './section/Chart'

const Home = ({navigation}: ComponentType) => {
  const [loadingAndError, setLoadingAndError] = useState(loadingAndErrorDefault)
  const [top5Countries, setTop5Countries] = useState<Array<any>>([])

  useEffect(() => {
    const fetchData = () => {
      const result = chartData.Countries.sort(
        (a, b) => b.TotalConfirmed - a.TotalConfirmed,
      ).slice(0, 5)
      console.log({result})
      setTop5Countries(result)
    }
    fetchData()
  }, [])

  return (
    <SafeAreaView style={[background.app_white, flex.flex_1]}>
      <Appbar title={`Home`} containerStyle={[padding.x_20]} />
      <FlatList
        data={[1]}
        renderItem={() => (
          <>
            <PCRCard onPress={() => {}} />
            <GlobalStatus
              metrics={metrics}
              totalCases={1000000}
              totalNewCases={30000}
            >
              <BarChartComponent />
            </GlobalStatus>

            <View style={[margin.t_20]}>
              <SectionBar
                title={`Affected Countries`}
                onPress={() => navigation.navigate(COUNTRY_LIST_SCREEN)}
                subText={`See all`}
                image={forward}
              />
              <FlatList
                data={top5Countries}
                showsVerticalScrollIndicator={false}
                ListHeaderComponent={
                  <TableRow
                    firstText={`Region`}
                    secondText={`Actives`}
                    thirdText={`Deaths`}
                    firstStyle={[font.regular]}
                    secondStyle={[color.text_black]}
                    thirdStyle={[color.text_black]}
                  />
                }
                renderItem={({item}) => (
                  <TableRow
                    firstText={item.Country}
                    secondText={`${item.TotalConfirmed}`}
                    thirdText={`${item.TotalDeaths}`}
                    onPress={() =>
                      navigation.navigate(DETAIL_SCREEN, {detail: item})
                    }
                  />
                )}
                ListEmptyComponent={
                  <Loader
                    centralize={true}
                    size={'small'}
                    loading={loadingAndError.loading}
                  />
                }
                contentContainerStyle={[flex.grow_1, height.min_full]}
                style={[styles.countriesTable]}
                keyExtractor={(item: any, index: number) =>
                  `list${item.CountryCode}`
                }
                listKey={`countries-table`}
              />
            </View>
            <View style={[margin.t_20]}>
              <SectionBar title={`Latest Updates`} subText={`June 03 2022`} />

              <FlatList
                data={newsData}
                showsVerticalScrollIndicator={false}
                renderItem={({item}) => (
                  <Pressable style={styles.newsTableButton}>
                    <CustomText text={item.text} />
                    <Image source={forward} style={[width[20], height[20]]} />
                  </Pressable>
                )}
                ListEmptyComponent={
                  <Loader
                    centralize={true}
                    size={'small'}
                    loading={loadingAndError.loading}
                  />
                }
                contentContainerStyle={[flex.grow_1, height.min_full]}
                style={[margin.t_10]}
                keyExtractor={(item, index: number) => `list${item.id}`}
                listKey={`news-table`}
              />
            </View>
          </>
        )}
        contentContainerStyle={[padding.x_20]}
      />
    </SafeAreaView>
  )
}

export default Home

const styles = {
  countriesTable: [
    flex.flex_1,
    background.pure_white,
    border.r_8,
    margin.t_10,
    padding.x_20,
    padding.y_20,
  ],
  newsTableButton: [
    height[49],
    border.w_thin,
    margin.y_5,
    background.pure_white,
    border.r_12,
    flex.alignCenter,
    flex.justifyBetween,
    padding.x_20,
    flex.row,
  ],
}
