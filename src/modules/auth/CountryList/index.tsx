import {
  FlatList,
  Image,
  Pressable,
  SafeAreaView,
  TextInput,
  View,
} from 'react-native'
import React, {useEffect, useState} from 'react'
import {background} from 'styles/background'
import {border, flex} from 'styles/layout'
import SectionBar from 'components/SectionBar'
import {height, padding, margin, width} from 'styles/spacing'
import {color, font} from 'styles/typography'
import Loader from 'components/Loader'
import TableRow from 'components/TableRow'
import BottomSheet from 'components/BottomSheet'
import {chartData, sortOption} from 'utils/data/values'
import {clear, forward, search} from 'utils/images/list'
import Appbar from 'components/Appbar'
import {ComponentType} from 'interface/ComponentType'
import {DETAIL_SCREEN} from 'navigation/constant'

const CountryList = ({navigation}: ComponentType) => {
  const [selectedSort, setSelectedSort] = useState(sortOption[0].value)
  const [query, setQuery] = useState('')
  const [showBottomSheet, setShowBottomSheet] = useState(false)

  useEffect(() => {
    const handleSortOperation = () => {}
    handleSortOperation()
  }, [selectedSort])

  const handleQueryInput = (text: string) => {
    setQuery(text)
  }

  return (
    <>
      <SafeAreaView style={[background.app_white, flex.flex_1]}>
        <View style={[flex.flex_1, padding.x_20]}>
          <Appbar showBack={true} onPress={() => navigation.goBack()} />
          <View style={styles.innerContainer}>
            <Image style={[width[17], height[17]]} source={search} />
            <TextInput
              value={query}
              onChangeText={handleQueryInput}
              style={[flex.flex_1, margin.x_10]}
            />
            <Pressable onPress={() => setQuery('')}>
              <Image style={[width[14], height[14]]} source={clear} />
            </Pressable>
          </View>
          <SectionBar
            title={`Affected Countries`}
            onPress={() => setShowBottomSheet(true)}
            subText={`Sort By`}
            image={forward}
          />

          <FlatList
            data={chartData.Countries}
            showsVerticalScrollIndicator={false}
            ListHeaderComponent={
              <TableRow
                firstText={`Region`}
                secondText={`Actives`}
                thirdText={`Deaths`}
                firstStyle={[font.regular]}
                secondStyle={[color.text_black]}
                thirdStyle={[color.text_black]}
              />
            }
            renderItem={({item}) => (
              <TableRow
                firstText={item.Country}
                secondText={`${item.TotalConfirmed}`}
                thirdText={`${item.TotalDeaths}`}
                onPress={() =>
                  navigation.navigate(DETAIL_SCREEN, {detail: item})
                }
              />
            )}
            ListEmptyComponent={
              <Loader centralize={true} size={'small'} loading={false} />
            }
            contentContainerStyle={[flex.grow_1, height.min_full]}
            style={styles.flatListStyle}
            keyExtractor={(item) => `list${item.CountryCode}`}
          />
        </View>
      </SafeAreaView>
      <BottomSheet
        showBottomSheet={showBottomSheet}
        setShowBottomSheet={setShowBottomSheet}
        selectedSort={selectedSort}
        setSelectedSort={setSelectedSort}
      />
    </>
  )
}

export default CountryList

const styles = {
  innerContainer: [
    margin.t_20,
    background.pure_white,
    flex.row,
    height[49],
    flex.alignCenter,
    padding.x_20,
    border.r_12,
  ],
  flatListStyle: [
    flex.flex_1,
    background.pure_white,
    border.r_8,
    margin.t_10,
    padding.x_20,
    padding.y_20,
  ],
}
