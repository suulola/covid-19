export const LANDING_SCREEN = `Landing`
export const HOME_SCREEN = `Home`
export const COUNTRY_LIST_SCREEN = `CountryList`
export const DETAIL_SCREEN = `Detail`
export const REPORT_SCREEN = `Report`