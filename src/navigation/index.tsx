import React from 'react'
import {NavigationContainer} from '@react-navigation/native'
import {createNativeStackNavigator} from '@react-navigation/native-stack'
import * as Routes from './constant'
import Home from 'modules/auth/Home'
import CountryList from 'modules/auth/CountryList'
import Detail from 'modules/auth/Detail'

const Stack = createNativeStackNavigator()

const NavigationProvider = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName={Routes.HOME_SCREEN}
        screenOptions={{headerShown: false}}
      >
        <Stack.Screen name={Routes.HOME_SCREEN} component={Home} />
        <Stack.Screen
          name={Routes.COUNTRY_LIST_SCREEN}
          component={CountryList}
        />
        <Stack.Screen name={Routes.DETAIL_SCREEN} component={Detail} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default NavigationProvider
