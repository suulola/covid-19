export const APP_WHITE = `#F8F8F8`
export const PURE_WHITE = `#FFFFFF`
export const CARD_BACKGROUND = `#3C53A4`
export const APP_BLUE = `#0065FF`
export const APP_DARK_BLACK = `#112950`
export const APP_GRAY = `#B2BAC6`
export const APP_RED = `#F33060`
export const APP_GREEN = `#0CE07A`
