import { StyleSheet } from 'react-native';
import { fontPixel } from 'utils/dimension/layout';
import { APP_BLUE, APP_DARK_BLACK, APP_GRAY, APP_GREEN, APP_RED, APP_WHITE } from './constant';

export const font = StyleSheet.create({
  light: { fontFamily: 'PlusJakartaSans-Light' },
  regular: { fontFamily: 'PlusJakartaSans-Regular' },
  medium: { fontFamily: 'PlusJakartaSans-Medium' },
  semiBold: { fontFamily: 'PlusJakartaSans-SemiBold' },
  bold: { fontFamily: 'PlusJakartaSans-Bold' },
});

export const color = StyleSheet.create({
  text_gray: { color: APP_GRAY },
  text_blue: { color: APP_BLUE },
  text_white: { color: APP_WHITE },
  text_black: { color: APP_DARK_BLACK },
  text_red: { color: APP_RED },
  text_green: { color: APP_GREEN },
});
export const font_size = StyleSheet.create({
  10: { fontSize: fontPixel(10) },
  11: { fontSize: fontPixel(11) },
  12: { fontSize: fontPixel(12) },
  14: { fontSize: fontPixel(14) },
  17: { fontSize: fontPixel(17) },
  20: { fontSize: fontPixel(20) },
  24: { fontSize: fontPixel(24) },
});

export const align = StyleSheet.create({
  left: { textAlign: 'left' },
  right: { textAlign: 'right' },
  center: { textAlign: 'center' },
});

export const transform = StyleSheet.create({
  capitalize: { textTransform: 'capitalize' },
});
export const line = StyleSheet.create({
  height_24: { lineHeight: 24 },
});