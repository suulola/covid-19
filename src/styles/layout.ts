import { StyleSheet } from 'react-native';
import { APP_WHITE } from './constant';

export const flex = StyleSheet.create({
  flex_1: { flex: 1 },
  row: { flexDirection: 'row' },
  column: { flexDirection: 'column' },
  grow_1: { flexGrow: 1 },
  justifyCenter: { justifyContent: 'center' },
  justifyBetween: { justifyContent: 'space-between' },
  justifyAround: { justifyContent: 'space-around' },
  justifyEnd: { justifyContent: 'flex-end' },
  justifyStart: { justifyContent: 'flex-start' },
  alignCenter: { alignItems: 'center' },
  alignStart: { alignItems: 'flex-start' },
  alignEnd: { alignItems: 'flex-end' },
  alignBase: { alignItems: 'baseline' },
  selfCenter: { alignSelf: 'center' },
});

export const border = StyleSheet.create({
  radius_top_0: { borderTopRightRadius: 0, borderTopLeftRadius: 0 },
  r_8: { borderRadius: 8 },
  r_10: { borderRadius: 10 },
  r_12: { borderRadius: 12 },
  w_0: { borderWidth: 0 },
  w_thin: { borderWidth: 0.1 },
  w_1: { borderWidth: 1 },
  w_2: { borderWidth: 2 },
  c_white: { borderColor: APP_WHITE },
  c_blue: { borderColor: `#DFEEFF` },
  c_grey: { borderColor: `#C4C4C4` },
});

export const position = StyleSheet.create({
  absolute: { position: 'absolute' },
  relative: { position: 'relative' },
});
