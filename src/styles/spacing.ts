import { StyleSheet } from 'react-native';
import { heightPixel, widthPixel } from 'utils/dimension/layout';


export const padding = StyleSheet.create({
  // top
  t_10_percent: { paddingTop: '10%' },
  t_10: { paddingTop: 10 },
  t_15: { paddingTop: 15 },
  t_20: { paddingTop: 20 },
  t_50: { paddingTop: 50 },

  // bottom
  b_0: { paddingBottom: 0 },
  b_10: { paddingBottom: 10 },
  b_15: { paddingBottom: 15 },
  b_20: { paddingBottom: 20 },
  b_40: { paddingBottom: 40 },

  // left
  l_0: { paddingLeft: 0 },
  l_5: { paddingLeft: 5 },
  l_10: { paddingLeft: 10 },
  l_20: { paddingLeft: 20 },
  l_40: { paddingLeft: 40 },
  // horizontal
  x_0: { paddingHorizontal: 0 },
  x_5: { paddingHorizontal: 5 },
  x_10: { paddingHorizontal: 10 },
  x_20: { paddingHorizontal: 20 },
  // vertical
  y_20: { paddingVertical: 20 },
  y_40: { paddingVertical: 40 },
  y_5: { paddingVertical: 5 },
  y_0: { paddingVertical: 0 },
  y_10: { paddingVertical: 10 },
  y_15: { paddingVertical: 15 },

  // right
  r_10: { paddingRight: 10 },
  r_20: { paddingRight: 20 },
});

export const margin = StyleSheet.create({
  zero: { marginBottom: 0, marginTop: 0, marginLeft: 0, marginRight: 0 },
  // bottom
  b_0: { marginBottom: 0 },
  b_2: { marginBottom: 2 },
  b_5: { marginBottom: 5 },
  b_10: { marginBottom: 10 },
  b_15: { marginBottom: 15 },
  b_20: { marginBottom: 20 },
  b_30: { marginBottom: 30 },
  b_40: { marginBottom: 40 },
  b_50: { marginBottom: 50 },
  // vertical
  y_5: { marginVertical: 5 },
  y_10: { marginVertical: 10 },
  y_20: { marginVertical: 20 },
  y_30: { marginVertical: 30 },

  // horizontal
  x_0: { marginHorizontal: 0 },
  x_5: { marginHorizontal: 5 },
  x_10: { marginHorizontal: 10 },
  x_20: { marginHorizontal: 20 },
  // top
  t_0: { marginTop: 0 },
  t_1: { marginTop: 1 },
  t_5: { marginTop: 5 },
  t_10: { marginTop: 10 },
  t_12: { marginTop: 12 },
  t_15: { marginTop: 15 },
  t_20: { marginTop: 20 },
  t_30: { marginTop: 30 },
  t_50: { marginTop: 50 },
  // right
  r_2: { marginRight: 2 },
  r_5: { marginRight: 5 },
  r_8: { marginRight: 8 },
  r_10: { marginRight: 10 },
  // left
  l_5: { marginLeft: 5 },
  l_10: { marginLeft: 10 },
});

export const width = StyleSheet.create({
  full: { width: '100%' },
  half: { width: '50%' },
  max_full: { minWidth: '100%' },
  min_full: { minWidth: '100%' },
  14: { width: widthPixel(14) },
  20: { width: widthPixel(20) },
  22: { width: widthPixel(22) },
  17: { width: widthPixel(17) },
  36: { width: widthPixel(36) },
  128: { width: widthPixel(128) },

});
export const height = StyleSheet.create({
  min_full: { minHeight: '100%' },
  max_full: { maxHeight: '100%' },
  full: { height: '100%' },
  1: { height: heightPixel(1) },
  14: { height: heightPixel(14) },
  17: { height: heightPixel(17) },
  20: { height: heightPixel(20) },
  36: { height: heightPixel(36) },
  49: { height: heightPixel(49) },
  175: { height: heightPixel(175) },



});

export const image = StyleSheet.create({
  aspect_1: { aspectRatio: 1 }
})