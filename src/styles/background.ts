import { StyleSheet } from 'react-native';
import { APP_WHITE, CARD_BACKGROUND, PURE_WHITE, APP_BLUE, APP_GRAY } from './constant';

export const background = StyleSheet.create({
  app_white: { backgroundColor: APP_WHITE },
  blue: { backgroundColor: APP_BLUE },
  pure_white: { backgroundColor: PURE_WHITE },
  card_bg: { backgroundColor: CARD_BACKGROUND },
  app_gray: { backgroundColor: APP_GRAY },
});
export const opacity = StyleSheet.create({
  faint: { opacity: 0.3 },
});